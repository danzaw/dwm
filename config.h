#include <stddef.h>
/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappih    = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 10;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=12" };
static const char dmenufont[]       = "monospace:size=11";
static unsigned int baralpha        = 0xd0;
static unsigned int borderalpha     = OPAQUE;
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray4[]       = "#eeeeee";


// rose pine
static const char col_foreground[] = "#e0def4";
static const char col_background[] = "#232136";
static const char col_foreground_alt[] = "#6e6a86";
static const char col_primary[] = "#3b3946";
static const char col_secondary[] = "#ea9a97";
static const char col_alert[] = "#c34043";
static const char col_black[] = "#2a273f";
static const char col_red[] = "#eb6f92";
static const char col_green[] = "#f6c177";
static const char col_yellow[] = "#ea9a97";
static const char col_blue[] = "#3e8fb0";
static const char col_magenta[] = "#c4a7e7";
static const char col_cyan[] = "#9ccfd8";
static const char col_white[] = "#6e6a86";


static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_foreground, col_background, col_gray2 },
	[SchemeSel]  = { col_foreground, col_red,  col_magenta  },
};

static const char *const autostart[] = {
	"wmname", "LG3D", NULL,
	"nm-applet", NULL,
	"nitrogen", "--restore", NULL,
	"dunst", NULL,
	"caffeine", NULL,
	"unclutter", "--timeout", "1", "--jitter", "25", "--ignore-scrolling", NULL,
    "setxkbmap", "-option", "caps:escape", NULL,
    "xset", "r", "rate", "250", "60", NULL,
    "xsettingsd", NULL,
    "solaar", "-w", "hide", NULL,
    "syncthing", "--no-browser", NULL,
	"picom", NULL,
    "udiskie", "--tray", NULL,
    "thunderbird", NULL,
    "rclone", "mount", "syncraft_at_google:/", "/home/himon/Remote/syncraft@google/", "--vfs-cache-mode", "full", "--daemon", NULL,
    /* "sh", "-c", "~/.config/polybar/launch.sh", NULL, */
	NULL /* terminate */
};

/* tagging */
static const char *tags[] = { "", "", "󰇌", "󰇍", "󰇎", "󰠮", "", "󰍡", "󰇮", "󰋎" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                        instance    title       tags mask     isfloating   monitor */
	{ "obsidian",                   NULL,       NULL,       1 << 5,       0,           -1 },
	{ "GG",                         NULL,       NULL,       1 << 7,       0,           -1 },
	{ "Gg",                         NULL,       NULL,       1 << 7,       0,           -1 },
	{ "microsoft teams - preview",  NULL,       NULL,       1 << 9,      0,           -1 },
	{ "Mail",                       NULL,       NULL,       1 << 8,       0,           -1 },
	{ "thunderbird",                NULL,       NULL,       1 << 8,       0,           -1 },
	{ NULL,                         NULL,       "cmus",     1 << 6,       0,           -1 },
	{ NULL,                         NULL,       "ncmpcpp",  1 << 6,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "TTT",      bstack },
	{ "---",      horizgrid },
	/* { "[M]",      monocle }, */
	/* { "[@]",      spiral }, */
	/* { "[\\]",     dwindle }, */
	/* { "H[]",      deck }, */
	/* { "===",      bstackhoriz }, */
	/* { "HHH",      grid }, */
	/* { "###",      nrowgrid }, */
	/* { ":::",      gaplessgrid }, */
	/* { "|M|",      centeredmaster }, */
	/* { ">M>",      centeredfloatingmaster }, */
	/* { "><>",      NULL },    /1* no layout function means floating behavior *1/ */
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-b", "-l", "10", "-p", "run:", NULL };
static const char *termcmd[]  = { "st", "-w", "''", "-e", "fish", NULL };
static const char *dmenuopeninterm[]  = { "/bin/sh", "-c", "~/.config/scripts/openinterminal.sh", NULL};

static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_t,      spawn,          {.v = dmenuopeninterm } },
	{ MODKEY|ControlMask,           XK_t,      spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	/* { MODKEY,                       XK_j,      focusstack,     {.i = +1 } }, */
	/* { MODKEY,                       XK_k,      focusstack,     {.i = -1 } }, */
	{ MODKEY,                       XK_j,      focusdown,     {} },
	{ MODKEY,                       XK_k,      focusup,     {} },
	{ MODKEY,                       XK_h,      focusleft,     {} },
	{ MODKEY,                       XK_l,      focusright,     {} },
	{ MODKEY|ShiftMask,                       XK_j,      movestack,     {.i = +1 } },
	{ MODKEY|ShiftMask,                       XK_k,      movestack,     {.i = -1 } },
	/* { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } }, */
	/* { MODKEY,                       XK_d,      incnmaster,     {.i = -1 } }, */
	{ MODKEY|ControlMask,           XK_j,      setmfact,       {.f = -0.05} },
	{ MODKEY|ControlMask,           XK_k,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY|Mod4Mask,              XK_u,      incrgaps,       {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_u,      incrgaps,       {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_i,      incrigaps,      {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_o,      incrogaps,      {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_o,      incrogaps,      {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_6,      incrihgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_7,      incrivgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_8,      incrohgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_9,      incrovgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_0,      togglegaps,     {0} },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
	/* { MODKEY,                       XK_Tab,    view,           {0} }, */
	{ ControlMask,                  XK_x,      killclient,     {0} },
	{ MODKEY,                       XK_Tab,      cyclelayout,      {.i = +1} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	/* { MODKEY,                       XK_0,      view,           {.ui = ~0 } }, */
	/* { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } }, */
	{ ControlMask,                  XK_comma,  focusmon,       {.i = -1 } },
	{ ControlMask,                  XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_comma,  viewprev,         {.i = -1 } },
	{ MODKEY,                       XK_period, viewnext,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	TAGKEYS(                        XK_0,                      9)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ MODKEY|ShiftMask,             XK_r,      quit,           {1} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
